﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arkanoid.Models.Bricks
{
    /// <summary>
    /// Normal brick without any special characteristics
    /// </summary>
    public class NormalBrick:Brick
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="gridX">Horizontal position of brick in grid of bricks, measured from the left</param>
        /// <param name="gridY">Vertical position of brick in grid of bricks, measured from the bottom</param>
        public NormalBrick(int gridX, int gridY, int roomHeight): base(gridX, gridY, roomHeight)
        {
            PointValue = 1;
        }
    }
}
