﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Arkanoid.Models.Bricks
{
    /// <summary>
    /// Brick that gets destroyed without the rebounce of the ball
    /// </summary>
    public class GlassBrick:Brick
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="gridX">Horizontal position of brick in grid of bricks, measured from the left</param>
        /// <param name="gridY">Vertical position of brick in grid of bricks, measured from the bottom</param>
        public GlassBrick(int gridX, int gridY, int roomHeight): base(gridX, gridY, roomHeight)
        {
            PointValue = 1;
        }
    }
}
