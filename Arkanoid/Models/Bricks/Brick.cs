﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arkanoid.Models.Bricks
{
    /// <summary>
    /// Class that defines common properties for all bricks
    /// </summary>
    public abstract class Brick
    {
        /// <summary>
        /// Amount of points player gets when this brick is destroyed
        /// </summary>
        public int PointValue { get; set; } = 0;

        /// <summary>
        /// Horizontal coordinate of the left edge of the brick
        /// </summary>
        public int LeftEdgeX { get; set; }
        /// <summary>
        /// Horizontal coordinate of the right edge of the brick
        /// </summary>
        public int RightEdgeX { get; set; }
        /// <summary>
        /// Vertical coordinate of the top edge of the brick
        /// </summary>
        public int TopEdgeY { get; set; }
        /// <summary>
        /// Vertical coordinate of the bottom edge of the brick
        /// </summary>
        public int BottomEdgeY { get; set; }

        /// <summary>
        /// Width of the brick
        /// </summary>
        protected const int Width = 50;
        /// <summary>
        /// Height of the brick
        /// </summary>
        protected const int Height = 17;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="gridX">Horizontal position of brick in grid of bricks, measured from the left</param>
        /// <param name="gridY">Vertical position of brick in grid of bricks, measured from the bottom</param>
        public Brick(int gridX, int gridY, int roomHeight)
        {
            LeftEdgeX = gridX * Width;
            RightEdgeX = (gridX + 1) * Width - 1;
            BottomEdgeY = roomHeight - (gridY + 1) * Height - 1;
            TopEdgeY = roomHeight - gridY * Height;
        }
    }
}
