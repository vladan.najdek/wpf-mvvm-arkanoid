﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Threading;

namespace Arkanoid.Models.Bricks
{
    /// <summary>
    /// Brick that periodically changes its Score value
    /// </summary>
    public class JokerBrick:Brick
    {
        /// <summary>
        /// Timer that controls the frequency of the changes of the PointValue 
        /// </summary>
        public DispatcherTimer ScoreChanger { get; set; }
        /// <summary>
        /// Collection of values between which the PointValue alternates
        /// </summary>
        public List<int> Scores { get; } = new List<int>() { 0, 1, 2, 5, 10 };
        /// <summary>
        /// Index of current PointValue in Scores collection
        /// </summary>
        public int ScoreIndex { get; private set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="gridX">Horizontal position of brick in grid of bricks, measured from the left</param>
        /// <param name="gridY">Vertical position of brick in grid of bricks, measured from the bottom</param>
        public JokerBrick(int gridX, int gridY, int roomHeight): base(gridX, gridY, roomHeight)
        {
            ScoreChanger = new DispatcherTimer();
            ScoreChanger.Interval = new TimeSpan(0, 0, 1);
            ScoreChanger.Tick += new EventHandler(ChangeScore);
            PointValue = Scores[0];
            ScoreChanger.Start();
        }

        /// <summary>
        /// Changes PointValue to the next one in the Scores collection
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void ChangeScore(object sender, EventArgs e)
        {
            ScoreIndex++;
            if (ScoreIndex == Scores.Count) ScoreIndex = 0;

            PointValue = Scores[ScoreIndex];
        }
    }
}
