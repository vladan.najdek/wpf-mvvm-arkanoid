﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Arkanoid.Models.Bricks
{
    /// <summary>
    /// Brick that requires two hits to be broken
    /// </summary>
    public class SteelBrick: Brick
    {
        private bool cracked;
        /// <summary>
        /// Indicates whether this brick was already hit
        /// </summary>
        public bool Cracked { get { return cracked; } set { cracked = value; CrackedChanged(); } }
        /// <summary>
        /// Event that is raised when the Cracked property changes (to either true and false)
        /// </summary>
        public event Action CrackedChanged = () => { };

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="gridX">Horizontal position of brick in grid of bricks, measured from the left</param>
        /// <param name="gridY">Vertical position of brick in grid of bricks, measured from the bottom</param>
        public SteelBrick(int gridX, int gridY, int roomHeight) : base(gridX, gridY, roomHeight)
        {
            PointValue = 2;
        }
    }
}
