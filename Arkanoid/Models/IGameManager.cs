﻿using Arkanoid.Models.Bricks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Arkanoid.Models
{
    /// <summary>
    /// Abstraction of game manager
    /// </summary>
    public interface IGameManager: INotifyPropertyChanged
    {
        /// <summary>
        /// Width of the game area
        /// </summary>
        public int LevelWidth { get; }
        /// <summary>
        /// Height of the game area
        /// </summary>
        public int LevelHeight { get; }

        /// <summary>
        /// Main ball in the game room
        /// </summary>
        public Ball Ball { get; }
        /// <summary>
        /// Main platform in game room
        /// </summary>
        public Pad Pad { get; }
        /// <summary>
        /// Collection of brick models in game room
        /// </summary>
        public List<Brick> Bricks { get; set; }
        /// <summary>
        /// Model for all player stats (lives, score, etc.)
        /// </summary>
        public Player Player { get; }

        /// <summary>
        /// Starts the game loop and launches the ball
        /// </summary>
        public void LaunchBall();

        /// <summary>
        /// Event raised every step of the game loop
        /// </summary>
        public event Action PositionsUpdated;
        /// <summary>
        /// Event raised when a brick is destroyed
        /// </summary>
        public event Action<Brick> RemovedBrick;
        /// <summary>
        /// Event raised when a new brick is created
        /// </summary>
        public event Action<Brick> AddedBrick;
    }
}
