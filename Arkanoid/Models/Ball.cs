﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arkanoid.Models
{
    /// <summary>
    /// Ball that destroys bricks
    /// </summary>
    public class Ball
    {
        /// <summary>
        /// Horizontal position of the bottom left corner of the ball
        /// </summary>
        public int PosX { get; set; }
        /// <summary>
        /// Vertical position of the bottom left corner of the ball
        /// </summary>
        public int PosY { get; set; }

        /// <summary>
        /// Horizontal velocity of the ball (positive is to the right)
        /// </summary>
        public int VectorX { get; set; }
        /// <summary>
        /// Vertical velocity of the ball (positive is to the top)
        /// </summary>
        public int VectorY { get; set; }

        /// <summary>
        /// Size of the ball
        /// </summary>
        public int Size { get; } = 16;
    }
}
