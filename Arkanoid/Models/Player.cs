﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Arkanoid.Models
{
    /// <summary>
    /// Model holding player data (score, lives etc.)
    /// </summary>
    public class Player: BasicPropertyChangedNotifier
    {
        private int score;
        /// <summary>
        /// Amount of points player has earned
        /// </summary>
        public int Score { get => score; set { score = value; OnPropertyChanged(nameof(Score)); } }

        private int lives;
        /// <summary>
        /// Amount of lives player has remaining
        /// </summary>
        public int Lives { get => lives; set { lives = value; OnPropertyChanged(nameof(Lives)); } }

        private int lastScore;
        /// <summary>
        /// Score of the last finished game
        /// </summary>
        public int LastScore { get => lastScore; private set { lastScore = value; OnPropertyChanged(nameof(LastScore)); } }

        private int bestScore;
        /// <summary>
        /// Score of the best finished game
        /// </summary>
        public int BestScore { get => bestScore; private set { bestScore = value; OnPropertyChanged(nameof(BestScore)); } }

        /// <summary>
        /// Reset lives and score. Writes current score to LastScore and possibly BestScore
        /// </summary>
        public void Reset()
        {
            LastScore = Score;
            if (LastScore > BestScore) BestScore = LastScore;
            Score = 0;
            Lives = 3;
        }
    }
}
