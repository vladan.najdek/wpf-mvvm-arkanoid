﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arkanoid.Models
{
    /// <summary>
    /// Moving platform that is controlled by player
    /// </summary>
    public class Pad
    {
        /// <summary>
        /// Horizontal position of the pad measured from the left
        /// </summary>
        public int PosX { get; set; }
        /// <summary>
        /// Height of the pad
        /// </summary>
        public int Height { get; } = 32;
        /// <summary>
        /// Width of the pad
        /// </summary>
        public int Width { get; } = 122;
    }
}
