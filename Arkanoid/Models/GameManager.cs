﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Windows.Threading;
using System.Windows.Input;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Controls;
using System.Linq;
using Arkanoid.Models.Bricks;
using System.Windows.Navigation;

namespace Arkanoid.Models
{
    /// <summary>
    /// Manages game states and game loop
    /// </summary>
    public class GameManager:BasicPropertyChangedNotifier, IGameManager
    {
        public int LevelWidth { get; } = 300;
        public int LevelHeight { get; } = 600;

        public Player Player { get; } = new Player();
        public Ball Ball { get; } = new Ball();
        public Pad Pad { get; } = new Pad();
        public List<Brick> Bricks { get; set; } = new List<Brick>();
        
        public event Action PositionsUpdated = () => { };
        public event Action<Brick> RemovedBrick = (brick) => { };
        public event Action<Brick> AddedBrick = (brick) => { };

        private readonly DispatcherTimer gameLoop;        

        /// <summary>
        /// Default constructor
        /// </summary>
        public GameManager()
        {
            // Prepare game loop
            gameLoop = new DispatcherTimer();
            gameLoop.Interval = new TimeSpan(0, 0, 0, 0, 5);
            gameLoop.Tick += new EventHandler(GameStep);

            // Move pad and ball to the center and reset lives/score
            ResetGame();
        }

        public void LaunchBall()
        {
            // Set initial ball parameters
            Ball.VectorX = -3;
            Ball.VectorY = 5;

            // Start game loop
            gameLoop.Start();
        }

        /// <summary>
        /// Resets the positions of ball and pad and stops the game loop
        /// </summary>
        private void ResetPositions()
        {
            gameLoop.Stop();
            Ball.PosX = LevelWidth/2 - Ball.Size/2;
            Ball.PosY = Pad.Height;
            Pad.PosX = LevelWidth/2 - Pad.Width/2;
        }

        /// <summary>
        /// Resets positions, player stats and bricks
        /// </summary>
        private void ResetGame()
        {
            Player.Reset();
            ResetPositions();
            PrepareBricks();
        }

        /// <summary>
        /// Creates bricks
        /// </summary>
        private void PrepareBricks()
        {
            Brick brick;
            // Create brick pattern
            for (int yy = 0; yy < 6; yy++)
            {
                for (int xx = 0; xx < 6; xx++)
                {
                    if (xx == 0 && yy == 0 || xx == 5 && yy == 0)
                    {
                        brick = new JokerBrick(xx, yy, LevelHeight);
                    }
                    else if (xx == 0 || xx == 5)
                    {
                        brick = new GlassBrick(xx, yy, LevelHeight);
                    }
                    else if (xx - yy == 0 || yy + xx == 5)
                    {
                        brick = new SteelBrick(xx, yy, LevelHeight);
                    }
                    else
                    {
                        brick = new NormalBrick(xx, yy, LevelHeight);
                    }
                    Bricks.Add(brick);
                    AddedBrick(brick);
                }
            }
        }

        /// <summary>
        /// Main game loop, calculates ball and pad positions, pad controls and collisions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GameStep(object sender, EventArgs e)
        {
            // Pad controls
            if (Keyboard.IsKeyDown(Key.Left) && Pad.PosX > 0) Pad.PosX = Math.Max(Pad.PosX - 5, 0);
            if (Keyboard.IsKeyDown(Key.Right) && Pad.PosX < LevelWidth - Pad.Width) Pad.PosX = Math.Min(Pad.PosX + 5, LevelWidth - Pad.Width);

            // Ball - update position
            Ball.PosX = Ball.PosX + Ball.VectorX;
            Ball.PosY = Ball.PosY + Ball.VectorY;

            // Ball - edge bouncing
            if (Ball.PosX < 0)
            {
                Ball.PosX = 0;
                Ball.VectorX = -1 * Ball.VectorX;
            }

            if (Ball.PosX > LevelWidth - Ball.Size)
            {
                Ball.PosX = LevelWidth - Ball.Size;
                Ball.VectorX = -1 * Ball.VectorX;
            }

            if (Ball.PosY > LevelHeight - Ball.Size)
            {
                Ball.PosY = LevelHeight - Ball.Size;
                Ball.VectorY = -1 * Ball.VectorY;
            }

            // Ball - pad bouncing
            if (Ball.PosY < Pad.Height && Ball.PosX <= Pad.PosX + Pad.Width && Ball.PosX + Ball.Size > Pad.PosX)
            {
                Ball.PosY = Pad.Height;
                Ball.VectorY = -1 * Ball.VectorY;

                // Slightly change direction when bouncing from moving pad
                if (Keyboard.IsKeyDown(Key.Left)) 
                {
                    if (Ball.VectorX >= 0) Ball.VectorY += 2;
                    else Ball.VectorY -= 2;
                    Ball.VectorX -= 2;
                }
                if (Keyboard.IsKeyDown(Key.Right))
                {
                    if (Ball.VectorX >= 0) Ball.VectorY -= 2;
                    else Ball.VectorY += 2;
                    Ball.VectorX += 2;
                }
            }

            // Ball - brick collision
            for (int ii = 0; ii< Bricks.Count; ii++)
            {
                Brick currentBrick = Bricks[ii];

                if (IsInCollisionWithBall(currentBrick))
                {
                    switch (currentBrick)
                    {
                        case NormalBrick nb:
                            HandleBounce(nb);
                            RemoveBrick(nb);
                            break;

                        case SteelBrick sb:
                            if (sb.Cracked) RemoveBrick(sb);
                            else sb.Cracked = true; 
                            HandleBounce(sb);
                            break;

                        case GlassBrick gb:
                            RemoveBrick(gb);
                            break;

                        case JokerBrick jb:
                            HandleBounce(jb);
                            RemoveBrick(jb);
                            break;
                    }
                }
            }
            
            // Ball - fell through
            if (Ball.PosY <= 0)
            {
                Player.Lives--;
                if (Player.Lives <= 0) ResetGame();
                else ResetPositions(); 
            }

            // Raise event to update position in UI
            PositionsUpdated();
        }

        /// <summary>
        /// Checks whether the ball is in collision with the specified brick
        /// </summary>
        /// <param name="brick">Tested brick</param>
        /// <returns></returns>
        private bool IsInCollisionWithBall(Brick brick)
        {
            if(Ball.PosY + Ball.Size > brick.BottomEdgeY && Ball.PosY < brick.TopEdgeY && Ball.PosX + Ball.Size > brick.LeftEdgeX && Ball.PosX < brick.RightEdgeX)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Calculates new ball vectors after a collision
        /// </summary>
        /// <param name="brick"></param>
        private void HandleBounce(Brick brick)
        {
            // Bounce - left and right edge
            if (Ball.PosX < brick.LeftEdgeX || Ball.PosX + Ball.Size > brick.RightEdgeX)
            {
                Ball.VectorX = -1 * Ball.VectorX;
            }

            // Bounce - top and bottom edge
            if (Ball.PosY < brick.BottomEdgeY || Ball.PosY + Ball.Size > brick.TopEdgeY)
            {
                Ball.VectorY = -1 * Ball.VectorY;
            }
        }

        /// <summary>
        /// Destroy brick
        /// </summary>
        /// <param name="brick">Brick to destroy</param>
        private void RemoveBrick(Brick brick)
        {
            Player.Score += brick.PointValue;
            Bricks.Remove(brick);
            RemovedBrick(brick);

            // Check for victory condition
            if (Bricks.Count == 0) ResetGame();
        }
    }
}
