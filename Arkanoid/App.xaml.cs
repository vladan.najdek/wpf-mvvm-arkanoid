﻿using Arkanoid.Models;
using Arkanoid.Viewmodels;
using Arkanoid.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Arkanoid
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var gm = new GameManager();
            var mainVM = new MainViewmodel(gm);
            var mainView = new MainWindow(mainVM);
            mainView.Show();
        }
    }
}
