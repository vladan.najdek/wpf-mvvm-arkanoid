﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Arkanoid.Models;

namespace Arkanoid.Viewmodels
{
    /// <summary>
    /// Viewmodel exposing player stats to the PlayerView
    /// </summary>
    public class PlayerViewmodel: BasicPropertyChangedNotifier
    {
        /// <summary>
        /// Amount of points player has earned
        /// </summary>
        public int Score { get => model.Score; }
        /// <summary>
        /// Amount of lives player has remaining
        /// </summary>
        public int Lives { get => model.Lives; }
        /// <summary>
        /// Score of the last finished game
        /// </summary>
        public int LastScore { get => model.LastScore; }
        /// <summary>
        /// Score of the best finished game
        /// </summary>
        public int BestScore { get => model.BestScore; }

        /// <summary>
        /// Instance of the model that this viewmodel owns
        /// </summary>
        private Player model;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="model"></param>
        public PlayerViewmodel(Player model)
        {
            this.model = model;
            model.PropertyChanged += RaisePropertyChanged;
        }

        /// <summary>
        /// Notify of changed property
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void RaisePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }
    }
}
