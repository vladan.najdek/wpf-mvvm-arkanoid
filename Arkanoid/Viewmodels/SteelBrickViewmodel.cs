﻿using Arkanoid.Models.Bricks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media.Imaging;

namespace Arkanoid.Viewmodels
{
    /// <summary>
    /// BrickViewmodel extension to support Cracked model property
    /// </summary>
    public class SteelBrickViewmodel : BrickViewmodel
    {
        /// <summary>
        /// Instance of the model that this viewmodel owns
        /// </summary>
        public new SteelBrick Model { get; set; }

        private Uri normalImage;
        private Uri crackedImage;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="model">Steel brick model</param>
        public SteelBrickViewmodel(SteelBrick model): base(model)
        {
            Model = model;
            Model.CrackedChanged += ChangeImageVariation;
            crackedImage = new Uri($"/Images/{Model.GetType().Name}Cracked.png", UriKind.Relative);
            normalImage = new Uri($"/Images/{Model.GetType().Name}.png", UriKind.Relative);
        }

        private void ChangeImageVariation()
        {
            ImageUri = Model.Cracked ? crackedImage : normalImage;
            OnPropertyChanged(nameof(ImageUri));
        }
    }
}
