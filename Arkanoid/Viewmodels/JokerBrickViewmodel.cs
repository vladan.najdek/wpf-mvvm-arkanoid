﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media.Imaging;
using Arkanoid.Models.Bricks;

namespace Arkanoid.Viewmodels
{
    class JokerBrickViewmodel:BrickViewmodel
    {
        /// <summary>
        /// Instance of the model that this viewmodel owns
        /// </summary>
        public new JokerBrick Model { get; set; }

        private List<Uri> brickImages = new List<Uri>();

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="model">Joker brick model</param>
        public JokerBrickViewmodel(JokerBrick model) : base(model)
        {
            Model = model;
            Model.ScoreChanger.Tick += new EventHandler(ChangeImageByScore);
            for(int ii = 0; ii < Model.Scores.Count; ii++)
            {
                brickImages.Add(new Uri($"/Images/{Model.GetType().Name}{Model.Scores[ii]}.png", UriKind.Relative));
            }
            ImageUri = brickImages[0];
        }

        /// <summary>
        /// Change the brick image to represent current score value
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void ChangeImageByScore(object sender, EventArgs e)
        {
            ImageUri = brickImages[Model.ScoreIndex];
            OnPropertyChanged(nameof(ImageUri));
        }
    }
}
