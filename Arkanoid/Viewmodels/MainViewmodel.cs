﻿using Arkanoid.Models;
using Arkanoid.Models.Bricks;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;

namespace Arkanoid.Viewmodels
{
    /// <summary>
    /// Viewmodel that exposes properties for the MainWindow
    /// </summary>
    public class MainViewmodel: BasicPropertyChangedNotifier
    {
        private readonly IGameManager gameManager;

        /// <summary>
        /// Horizontal coordinate of the ball (measured from the left) 
        /// </summary>
        public int BallX { get => gameManager.Ball.PosX; }
        /// <summary>
        /// Vertical coordinate of the ball (measured from the bottom)
        /// </summary>
        public int BallY { get => gameManager.Ball.PosY; }
        /// <summary>
        /// Horizontal coordinate of the pad (measured from the left)
        /// </summary>
        public int PadX { get => gameManager.Pad.PosX; }

        /// <summary>
        /// Width of the game area
        /// </summary>
        public int LevelWidth { get => gameManager.LevelWidth; }
        /// <summary>
        /// Height of the game area
        /// </summary>
        public int LevelHeight { get => gameManager.LevelHeight; }

        /// <summary>
        /// Minimal height of the entire game window
        /// </summary>
        public int WindowMinHeight { get => (int)(1.1 * gameManager.LevelHeight); }
        /// <summary>
        /// Minimal width of the entire game window
        /// </summary>
        public int WindowMinWidth { get => (int)(1.1 * gameManager.LevelWidth); }

        private bool showControls = true;
        /// <summary>
        /// Display how to control the game to the player
        /// </summary>
        public bool ShowControls { get => showControls; private set { showControls = value; OnPropertyChanged(nameof(ShowControls)); } }

        /// <summary>
        /// Viewmodel for all player stats (lives, score, etc.)
        /// </summary>
        public PlayerViewmodel PlayerVM { get; }
        /// <summary>
        /// Collection of viewmodels representing various kinds of bricks
        /// </summary>
        public ObservableCollection<BrickViewmodel> Bricks { get; } = new ObservableCollection<BrickViewmodel>();

        /// <summary>
        /// Command to start the game and launch the ball (Default binding is Space key)
        /// </summary>
        public ICommand LaunchBallCmd { get; set; }
        
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="gameManagerModel">Responsible for managing game state</param>
        public MainViewmodel(IGameManager gameManagerModel)
        {
            gameManager = gameManagerModel ?? throw new ArgumentNullException();

            LaunchBallCmd = new RelayCommand(StartGame);
            PlayerVM = new PlayerViewmodel(gameManager.Player);
            gameManager.PositionsUpdated += UpdatePositions;
            gameManager.PropertyChanged += RaisePropertyChanged;
            gameManager.RemovedBrick += RemoveBrickVM;
            gameManager.AddedBrick += AddBrickVM;

            // During first run, AddedBrick event fired before VM subscribed
            foreach (Brick brick in gameManager.Bricks)
            {
                AddBrickVM(brick);
            }
        }

        private void StartGame()
        {
            ShowControls = false;
            gameManager.LaunchBall();
        }

        /// <summary>
        /// Creates brick viewmodel based on brick model type
        /// </summary>
        /// <param name="brick">Brick model</param>
        private void AddBrickVM(Brick brick)
        {
            switch (brick)
            {
                case SteelBrick sb:
                    Bricks.Add(new SteelBrickViewmodel(sb));
                    break;
                case JokerBrick jb:
                    Bricks.Add(new JokerBrickViewmodel(jb));
                    break;
                default:
                    Bricks.Add(new BrickViewmodel(brick));
                    break;
            }
        }

        /// <summary>
        /// Removes brick viewmodel based on brick model
        /// </summary>
        /// <param name="oldBrick">Brick model</param>
        private void RemoveBrickVM(Brick oldBrick)
        {
            if (oldBrick == null) throw new ArgumentNullException();

            var vm = Bricks.First(b => b.Model == oldBrick);
            Bricks.Remove(vm);
        }

        /// <summary>
        /// Update property in view
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        private void RaisePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Update properties in view that are calculated every game step
        /// </summary>
        private void UpdatePositions()
        {
            OnPropertyChanged(nameof(BallX));
            OnPropertyChanged(nameof(BallY));
            OnPropertyChanged(nameof(PadX));
        }
    }
}
