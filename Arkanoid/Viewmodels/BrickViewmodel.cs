﻿using Arkanoid.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Arkanoid.Models.Bricks;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Diagnostics;

namespace Arkanoid.Viewmodels
{
    public class BrickViewmodel: BasicPropertyChangedNotifier
    {
        /// <summary>
        /// Instance of the model that this viewmodel owns
        /// </summary>
        public Brick Model { get; }
        /// <summary>
        /// Relative uri of the brick image
        /// </summary>
        public Uri ImageUri { get; protected set; }
        /// <summary>
        /// Horizontal coordinate of the left brick edge (measured from the left edge of the room)
        /// </summary>
        public int LeftEdgeX { get => Model.LeftEdgeX; }
        /// <summary>
        /// Vertical coordinate of the bottom brick edge (measured from the bottom edge of the room)
        /// </summary>
        public int BottomEdgeY { get => Model.BottomEdgeY; }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="model">Brick model</param>
        public BrickViewmodel(Brick model)
        {
            Model = model ?? throw new ArgumentNullException();
            ImageUri = new Uri($"/Images/{Model.GetType().Name}.png", UriKind.Relative);
        }
    }
}
