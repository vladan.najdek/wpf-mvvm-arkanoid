﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Arkanoid
{
    /// <summary>
    /// Basic implementation of INotifyPropertyChanged
    /// </summary>
    public abstract class BasicPropertyChangedNotifier : INotifyPropertyChanged
    {
        /// <summary>
        /// The event that is fired when any child property changes its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        /// <summary>
        /// Call this method to raise a <see cref="PropertyChanged"/> event
        /// </summary>
        /// <param name="name">Name of the property that changed</param>
        public void OnPropertyChanged(string name)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
